<?php
namespace App\models;
use Illuminate\Database\Eloquent\Model;

class Container extends Model
{

public $table = "container";

protected $fillable = ['container_id','container_name'];

public static $rules = array(

		'container_name'=>'required',

	);

}
